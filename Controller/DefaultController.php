<?php

namespace Mnm\MnmUserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DefaultController extends Controller {

    /**
     * @Route("/" , name="mnm_user_index")
     * @Template()
     */
    public function indexAction() {
        return array();
    }

}
