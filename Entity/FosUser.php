<?php

namespace Mnm\MnmUserBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

class FosUser extends BaseUser {

    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    private $facebook_id;

    /**
     * @var string
     */
    private $facebookAccessToken;

    /**
     * @var string
     */
    private $google_id;

    /**
     * @var string
     */
    private $googleAccessToken;

    /**
     * @var string
     */
    private $linkedinId;

    /**
     * @var string
     */
    private $linkedinAccessToken;

    /**
     * @var string
     */
    private $hotmailId;

    /**
     * @var string
     */
    private $hotmailAccessToken;

    /**
     * @var string
     */
    private $yahoo_id;

    /**
     * @var string
     */
    private $yahooAccessToken;

    /**
     * @var string
     */
    private $pictureUrl;

    /**
     * @var string
     */
    private $FirstName;

    /**
     * @var string
     */
    private $LastName;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set facebook_id
     *
     * @param string $facebookId
     * @return FosUser
     */
    public function setFacebookId($facebookId)
    {
        $this->facebook_id = $facebookId;
    
        return $this;
    }

    /**
     * Get facebook_id
     *
     * @return string 
     */
    public function getFacebookId()
    {
        return $this->facebook_id;
    }

    /**
     * Set facebookAccessToken
     *
     * @param string $facebookAccessToken
     * @return FosUser
     */
    public function setFacebookAccessToken($facebookAccessToken)
    {
        $this->facebookAccessToken = $facebookAccessToken;
    
        return $this;
    }

    /**
     * Get facebookAccessToken
     *
     * @return string 
     */
    public function getFacebookAccessToken()
    {
        return $this->facebookAccessToken;
    }

    /**
     * Set google_id
     *
     * @param string $googleId
     * @return FosUser
     */
    public function setGoogleId($googleId)
    {
        $this->google_id = $googleId;
    
        return $this;
    }

    /**
     * Get google_id
     *
     * @return string 
     */
    public function getGoogleId()
    {
        return $this->google_id;
    }

    /**
     * Set googleAccessToken
     *
     * @param string $googleAccessToken
     * @return FosUser
     */
    public function setGoogleAccessToken($googleAccessToken)
    {
        $this->googleAccessToken = $googleAccessToken;
    
        return $this;
    }

    /**
     * Get googleAccessToken
     *
     * @return string 
     */
    public function getGoogleAccessToken()
    {
        return $this->googleAccessToken;
    }

    /**
     * Set linkedinId
     *
     * @param string $linkedinId
     * @return FosUser
     */
    public function setLinkedinId($linkedinId)
    {
        $this->linkedinId = $linkedinId;
    
        return $this;
    }

    /**
     * Get linkedinId
     *
     * @return string 
     */
    public function getLinkedinId()
    {
        return $this->linkedinId;
    }

    /**
     * Set linkedinAccessToken
     *
     * @param string $linkedinAccessToken
     * @return FosUser
     */
    public function setLinkedinAccessToken($linkedinAccessToken)
    {
        $this->linkedinAccessToken = $linkedinAccessToken;
    
        return $this;
    }

    /**
     * Get linkedinAccessToken
     *
     * @return string 
     */
    public function getLinkedinAccessToken()
    {
        return $this->linkedinAccessToken;
    }

    /**
     * Set hotmailId
     *
     * @param string $hotmailId
     * @return FosUser
     */
    public function setHotmailId($hotmailId)
    {
        $this->hotmailId = $hotmailId;
    
        return $this;
    }

    /**
     * Get hotmailId
     *
     * @return string 
     */
    public function getHotmailId()
    {
        return $this->hotmailId;
    }

    /**
     * Set hotmailAccessToken
     *
     * @param string $hotmailAccessToken
     * @return FosUser
     */
    public function setHotmailAccessToken($hotmailAccessToken)
    {
        $this->hotmailAccessToken = $hotmailAccessToken;
    
        return $this;
    }

    /**
     * Get hotmailAccessToken
     *
     * @return string 
     */
    public function getHotmailAccessToken()
    {
        return $this->hotmailAccessToken;
    }

    /**
     * Set yahooId
     *
     * @param string $yahooId
     * @return FosUser
     */
    public function setYahooId($yahooId)
    {
        $this->yahoo_id = $yahooId;
    
        return $this;
    }

    /**
     * Get yahooId
     *
     * @return string 
     */
    public function getYahooId()
    {
        return $this->yahoo_id;
    }

    /**
     * Set yahooAccessToken
     *
     * @param string $yahooAccessToken
     * @return FosUser
     */
    public function setYahooAccessToken($yahooAccessToken)
    {
        $this->yahooAccessToken = $yahooAccessToken;
    
        return $this;
    }

    /**
     * Get yahooAccessToken
     *
     * @return string 
     */
    public function getYahooAccessToken()
    {
        return $this->yahooAccessToken;
    }

    /**
     * Set pictureUrl
     *
     * @param string $pictureUrl
     * @return FosUser
     */
    public function setPictureUrl($pictureUrl)
    {
        $this->pictureUrl = $pictureUrl;
    
        return $this;
    }

    /**
     * Get pictureUrl
     *
     * @return string 
     */
    public function getPictureUrl()
    {
        return $this->pictureUrl;
    }

    /**
     * Set FirstName
     *
     * @param string $firstName
     * @return FosUser
     */
    public function setFirstName($firstName)
    {
        $this->FirstName = $firstName;
    
        return $this;
    }

    /**
     * Get FirstName
     *
     * @return string 
     */
    public function getFirstName()
    {
        return $this->FirstName;
    }

    /**
     * Set LastName
     *
     * @param string $lastName
     * @return FosUser
     */
    public function setLastName($lastName)
    {
        $this->LastName = $lastName;
    
        return $this;
    }

    /**
     * Get LastName
     *
     * @return string 
     */
    public function getLastName()
    {
        return $this->LastName;
    }
    /**
     * @var string
     */


    /**
     * @var integer
     */
    private $uiEduUin;


    /**
     * Set uiEduUin
     *
     * @param integer $uiEduUin
     * @return FosUser
     */
    public function setUiEduUin($uiEduUin)
    {
        $this->uiEduUin = $uiEduUin;

        return $this;
    }

    /**
     * Get uiEduUin
     *
     * @return integer 
     */
    public function getUiEduUin()
    {
        return $this->uiEduUin;
    }
}
