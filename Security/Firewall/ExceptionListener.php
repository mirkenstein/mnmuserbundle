<?php
namespace Mnm\MnmUserBundle\Security\Firewall;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\Firewall\ExceptionListener as BaseExceptionListener;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\SecurityContext;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Router;



class ExceptionListener extends BaseExceptionListener
{



    protected function setTargetPath(Request $request)
    {
  //       $referer = $request->getSession()->get('_security.main.target_path');
  
        // Do not save target path for XHR and non-GET requests
        // You can add any more logic here you want
        if ($request->isXmlHttpRequest() || 'GET' !== $request->getMethod()) {
            return;
        }
// error_log("called");
// $request = Request::createFromGlobals();
// $request = Request::create(
//     '/spec');
//error_log($request);
        parent::setTargetPath($request);
    }
}

?>