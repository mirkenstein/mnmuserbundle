<?php

namespace Mnm\MnmUserBundle\Form\Type;

/**
 * Overriding Default FOSUserBundle Forms  
 *
 * @author mnm
 */
use Symfony\Component\Form\FormBuilderInterface;
use FOS\UserBundle\Form\Type\RegistrationFormType as BaseType;

class RegistrationFormType extends BaseType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        parent::buildForm($builder, $options);

        // add your custom field
        $builder->add('email', 'email', array('label' => 'form.email', 'translation_domain' => 'FOSUserBundle', 'attr' => array('class' => 'form-control', 'placeholder' => 'form.email')))
                ->add('username', null, array('label' => 'form.username', 'translation_domain' => 'FOSUserBundle', 'translation_domain' => 'FOSUserBundle', 'attr' => array('class' => 'form-control', 'placeholder' => 'form.username')))
                ->add('plainPassword', 'repeated', array(
                    'attr' => array('class' => 'form-control'),
                    'type' => 'password',
                    'options' => array('translation_domain' => 'FOSUserBundle'),
                    'first_options' => array('label' => 'form.password', 'attr' => array('class' => 'form-control', 'placeholder' => 'form.password')),
                    'second_options' => array('label' => 'form.password_confirmation', 'attr' => array('class' => 'form-control', 'placeholder' => 'form.password_confirmation')),
                    'invalid_message' => 'fos_user.password.mismatch'
                ))
                ->add('first_name', 'text', array('label' => 'form.first_name', 'translation_domain' => 'FOSUserBundle', 'attr' => array('class' => 'form-control', 'placeholder' => 'form.first_name')))
                ->add('last_name', 'text', array('label' => 'form.last_name', 'translation_domain' => 'FOSUserBundle', 'attr' => array('class' => 'form-control', 'placeholder' => 'form.last_name')));
    }

    public function getName() {
        return 'mnm_user_registration';
    }

}

?>
