Mnm User Bundle
========================

This document contains information on how to setup this  bundle.

1) Installing as git submodule in you project src dir
------------------------------------------------------
Make sure your project is already in a git repo.  
From the root of your project add the submodule.  

           git submodule add  git@bitbucket.org:mirkenstein/mnmuserbundle.git ./src/Mnm/MnmUserBundle

2) Add in your project `composer.json` these two bundles
--------------------------------------------
    "require": {
            //...
            "friendsofsymfony/user-bundle": "dev-master",
            "hwi/oauth-bundle": "0.4.*@dev"
          }

3) Register the bundles to your application kernel
--------------------------------------------
        // app/AppKernel.php
        public function registerBundles()
        {
            return array(
                // ...
                new FOS\UserBundle\FOSUserBundle(),
                new HWI\Bundle\OAuthBundle\HWIOAuthBundle(),
                new Mnm\MnmUserBundle\MnmUserBundle(),
                // ...
            );
        }

3) Update your routing 
--------------------------------------------
      //app/config/routing.yml
       mnm_user:          
            resource: "@MnmUserBundle/Resources/config/routing.yml"

All the routes for this bundle are under `/user` prefix

4) Update the main security.yml
--------------------------------------------
        //app/config/secutiry.yml
        security:
    encoders:
        FOS\UserBundle\Model\UserInterface: sha512
        

    role_hierarchy:
        ROLE_REQUESTER: ROLE_USER 
        ROLE_PI: [ROLE_USER, ROLE_REQUESTER]
        ROLE_ACCOUNITING: [ROLE_USER, ROLE_REQUESTER, ROLE_PI]
        ROLE_ADMIN: [ ROLE_USER, ROLE_REQUESTER, ROLE_PI, ROLE_ACCOUNITING ]
        ROLE_SUPER_ADMIN: [ROLE_USER, ROLE_ADMIN, ROLE_ALLOWED_TO_SWITCH]

    providers:
        fos_userbundle:
            id: fos_user.user_provider.username_email
 
    firewalls:
        main:
            pattern: ^/
            form_login:
                provider: fos_userbundle
                csrf_provider: form.csrf_provider
                login_path: /login
                check_path: /login_check
            oauth:
                resource_owners:
                    facebook:           "/login/check-facebook"
                    google:             "/login/check-google"
                    yahoo:              "/login/check-yahoo"
                login_path:        /login
                failure_path:      /login
 
                oauth_user_provider:
                    #this is my custom user provider, created from FOSUBUserProvider - will manage the
                    #automatic user registration on your site, with data from the provider (facebook. google, etc.)
                    service: mnm_user_provider
            logout:       true
            anonymous:    true
 
        login:
            pattern:  ^/login$
            security: false
 
            remember_me:
                key: "%secret%"
                lifetime: 600 # units seconds
                path: /
                domain: ~ # Defaults to the current domain from $_SERVER
    access_control:
        - { path: ^/$, roles: ROLE_USER }
        - { path: ^/login, roles: IS_AUTHENTICATED_ANONYMOUSLY, requires_channel: https }





5) Update your main config.yml
--------------------------------------------
    //include the bundle services
    - { resource: "@MnmUserBundle/Resources/config/services.yml" }
    //assetic add the bundle
            assetic:
                bundles:        [ MnmUserBundle ]
    // enable translations
    translator:      { fallback: "%locale%" }
    //fos user
            fos_user:
                db_driver: orm # other valid values are 'mongodb', 'couchdb' and 'propel'
                firewall_name: main
                user_class: Mnm\MnmUserBundle\Entity\FosUser 
                registration:
                    confirmation:
                        enabled:    true
                    form:
                      type: mnm_user_registration
                profile:
                  form:
                      type: mnm_user_profile

            hwi_oauth:
                connect:
                    account_connector: mnm_user_provider
                firewall_name: main
                fosub:
                    username_iterations: 30
                    properties:
                        # these properties will be used/redefined later in the custom FOSUBUserProvider service.
                        facebook: facebook_id
                        google: google_id
                        yahoo: yahoo_id
                resource_owners:
                    facebook:
                        type:                facebook
                        client_id:           %facebook_client_id%
                        client_secret:       %facebook_client_secret%
                        infos_url:           https://graph.facebook.com/me?fields=email,id,link,name,username,verified,picture.type(square)
                        scope:               "email"
                        paths:
                            email:           email
                            profilepicture:  picture.data.url
                    google:
                        type:                google
                        client_id:           %google_client_id%
                        client_secret:       %google_client_secret%
                        scope:               "https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile"
                    yahoo:
                        type:                yahoo
                        client_id:           %yahoo_client_id%
                        client_secret:       %yahoo_client_secret%
                        scope:               "read" 
                        paths:
                          email:             profile.emails.0.handle
                          profilepicture:    profile.image.imageUrl

6) Add the respective parameters to your parameters.yml parameters.yml.dist
-----------------------------------------------------------------------------

    facebook_client_id: 51134345454545
    facebook_client_secret: 883abfffd6advsdga
    yahoo_client_secret: 9edb9666b7fb2642ad59sdsafsdf
    yahoo_client_id: dj0yJmk9YkFrUmdBWnZMWVFkJmQ9WVdrOWVIZDFRMjVFTXpJbWNHbzASDSF
    google_client_id: 345765753456-3465jpis1225mpc6pqjk6qrvagkkaea.apps.googleusercontent.com
    google_client_secret: B_VSDGDASDF-lnesfrergt