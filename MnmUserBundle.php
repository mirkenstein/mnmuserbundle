<?php

namespace Mnm\MnmUserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class MnmUserBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}